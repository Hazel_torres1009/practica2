<?php
	include_once 'conexion.php';

	if(isset($_GET['id'])){
		$id=(int) $_GET['id'];

		$buscar_id=$con->prepare('SELECT * FROM clientes WHERE id=:id LIMIT 1');
		$buscar_id->execute(array(
			':id'=>$id
		));
		$resultado=$buscar_id->fetch();
	}else{
		header('Location: index.php');
	}


	if(isset($_POST['guardar'])){
		$nombre=$_POST['nombre'];
		$caducidad=$_POST['caducidad'];
		$cantidad=$_POST['cantidad'];
		$costo=$_POST['costo'];
		$id=(int) $_GET['id'];

		if(!empty($nombre) && !empty($caducidad) && !empty($cantidad) && !empty($costo) ){
			if(!filter_var($costo,FILTER_VALIDATE_INT)){
				echo "<script> alert('Costo no valido');</script>";
			}else{
				$consulta_update=$con->prepare(' UPDATE clientes SET  
					nombre=:nombre,
					caducidad=:caducidad,
					cantidad=:cantidad,
					costo=:costo 
					WHERE id=:id;'
				);
				$consulta_update->execute(array(
					':nombre' =>$nombre,
					':caducidad' =>$caducidad,
					':cantidad' =>$cantidad,
					':costo' =>$costo,
					':id' =>$id
				));
				header('Location: index.php');
			}
		}else{
			echo "<script> alert('Los campos estan vacios');</script>";
		}
	}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Editar Producto</title>
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	<div class="contenedor">
		<h2>CRUD DE INVENTARIO "LA CHOSA"</h2>
		<form action="" method="post">
			<div class="form-group">
				<input type="text" name="nombre" value="<?php if($resultado) echo $resultado['nombre']; ?>" class="input__text">
				<input type="text" name="caducidad" value="<?php if($resultado) echo $resultado['caducidad']; ?>" class="input__text">
			</div>
			<div class="form-group">
				<input type="text" name="cantidad" value="<?php if($resultado) echo $resultado['cantidad']; ?>" class="input__text">
				<input type="text" name="costo" value="<?php if($resultado) echo $resultado['costo']; ?>" class="input__text">
			</div>
			<div class="btn__group">
				<a href="index.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div>
</body>
</html>
