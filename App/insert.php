<?php 
	include_once 'conexion.php';
	
	if(isset($_POST['guardar'])){
		$nombre=$_POST['nombre'];
		$caducidad=$_POST['caducidad'];
		$cantidad=$_POST['cantidad'];
		$costo=$_POST['costo'];

		if(!empty($nombre) && !empty($caducidad) && !empty($cantidad) && !empty($costo)){
			if(!filter_var($costo,FILTER_VALIDATE_INT)){
				echo "<script> alert('Costo no valido');</script>";
			}else{
				$consulta_insert=$con->prepare('INSERT INTO clientes(nombre,caducidad,cantidad,costo) VALUES(:nombre,:caducidad,:cantidad,:costo)');
				$consulta_insert->execute(array(
					':nombre' =>$nombre,
					':caducidad' =>$caducidad,
					':cantidad' =>$cantidad,
					':costo' =>$costo
				));
				header('Location: index.php');
			}
		}else{
			echo "<script> alert('Los campos estan vacios');</script>";
		}

	}


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Nuevo Producto</title>
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	<div class="contenedor">
		<h2>CRUD DE INVENTARIO "LA CHOSA"</h2>
		<form action="" method="post">
			<div class="form-group">
				<input type="text" name="nombre" placeholder="Nombre" class="input__text">
				<input type="text" name="caducidad" placeholder="Caducidad" class="input__text">
			</div>
			<div class="form-group">
				<input type="text" name="cantidad" placeholder="Cantidad" class="input__text">
                <input type="text" name="costo" placeholder="Costo" class="input__text">
			</div>
			<div class="btn__group">
				<a href="index.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div>
</body>
</html>
